// THIS IS ANGULAR JS
"use strict";

(function () {
  "use strict";
  app.controller('TestController', TestController)

  // INJECTING $http
  TestController.inject = ["$http"]

  function TestController($http) {
    var self = this;
    self.loaded = true
  }

}
)()