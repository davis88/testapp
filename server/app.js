const express = require('express')
const app = express()

app.use(express.static(__dirname + "/../client/"));

app.get('/api/someGetAPI', function (req, res) {
  res.status(200).send("Dummy")
})

app.post('/api/somePostAPI', function (req, res) {
    var data = req.body.data
    res.send(data)
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})